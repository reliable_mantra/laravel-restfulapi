## RESTful API Laravel

* Response transformations using Fractal
* Sorting, filtering and paging using Collections
* Caching responses
* HATEOAS Hypermedia Controls (using Fractal transformations)
* API authentication using Laravel Passport
* Scopes
* Final security layer using Policies and Gates
* CORS

## Laravel Passport OAuth
Create a client using this Artisan command: **php artisan passport:client**

Authorize URL: **/oauth/authorize**

Get token from: **/oauth/token**

## Client Credentials Grant Authentication
Used mostly for machine-to-machine communication.

Create a client credentials grant client: **php artisan passport:client --client**

Endpoint: **/oauth/token (POST)**

Request:

* grant_type (client_credentials)
* client_id
* client_secret
* scope (optional)

Response (JSON):

```json
{  
    "token_type": "Bearer",  
    "expires_in": 1800,  
    "access_token": ""  
}
```  

## Password Grant Authentication
Create a password grant client: **php artisan passport:client --password**

Endpoint: **/oauth/token (POST)**

Request:

* grant_type (password)
* client_id
* client_secret
* username
* password
* scope (optional)

Response (JSON):

```json
{  
    "token_type": "Bearer",  
    "expires_in": 1800,  
    "access_token": "",  
    "refresh_token": ""  
}
```

## Personal Grant Authentication
Should be used only for testing purposes, sometimes used to skip full oauth2 dance when working with JS.

Create a personal grant client: **php artisan passport:client --personal**

Once you have created a personal access client, you may issue tokens for a given user using the **createToken** method on the **User** model instance.
The **createToken** method accepts the name of the token as its first argument and an **optional array of scopes** as its second argument.

## Authorization Code Grant Authentication (Requires User Consent)
Create a client: **php artisan passport:client**

* User ID
* Client Name
* Redirect URL

First, you need to authorize: **/oauth/authorize (GET)**

* client_id
* redirect_uri
* response_type (code)

When successfully authorized, you will be redirected to the agreed upon **redirect_url** and you will get the **Authorization Code** which you need to store somewhere.
The authorization code is only valid for approximately 30 seconds.

Response: **redirect_uri/?code=__&state=**

Then, you need to authenticate: **/oauth/token (POST)**

* grant_type (authorization_code)
* client_id
* client_secret
* redirect_uri
* code (the retrieved authorization code)

Response (JSON):

```json
{  
    "token_type": "Bearer",  
    "expires_in": 1800,  
    "access_token": "",    
    "refresh_token": ""  
}
```

## Implicit Grant Authentication (Requires User Consent)
The Implicit grant type is optimized for public clients, such as those implemented in javascript or on mobile devices, where client credentials cannot be stored.  
This is why the response will contain the access_token in the url hash instead of a url query parameter.

Create a client: **php artisan passport:client**

* User ID
* Client Name
* Redirect URL

Authorize using this url: **/oauth/authorize (GET)**

* client_id
* redirect_uri
* response_type (token)

When successfully authorized, you will be redirected to the agreed upon **redirect_uri** and you will get a **Access Token** which you need to store somewhere.

Response: **redirect_uri/#access_token=__&expires_in=1800&token_type=Bearer**

## Refreshing tokens
Authenticate: **/oauth/token (POST)**

* grant_type (refresh_token)
* client_id
* client_secret
* refresh_token

Response (JSON):

```json
{  
    "token_type": "Bearer",  
    "expires_in": 1800,  
    "access_token": "",    
    "refresh_token": ""  
}
```

## OpenID Connect
Documentation explainer URL: https://medium.com/@darutk/diagrams-of-all-the-openid-connect-flows-6968e3990660

Following the OpenID documentation, to get the OpenId Connect id_token you must include the openid scope in the authorization request.
Allowed flows that should return id_token are:

* authorize with **response_type: code** and **scope: opeind** and then receive the id_token with the access_token
* or, authorize with response_type: id_token and receive the id_token right away
