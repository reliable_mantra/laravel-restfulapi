<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

abstract class AbstractTransformer extends TransformerAbstract
{
    /**
     * Mapped original model data to the new transformed model
     *
     * @var
     */
    protected static $map;
}
