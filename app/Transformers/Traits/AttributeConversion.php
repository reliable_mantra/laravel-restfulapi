<?php

namespace App\Transformers\Traits;

trait AttributeConversion
{
    /**
     * Get original model data attribute
     * using the transformed index
     *
     * @param $index
     * @return null
     */
    public static function originalAttribute($index)
    {
        $attributes = self::$map;

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    /**
     * Get transformed model data attribute
     * using the original index
     *
     * @param $index
     * @return null
     */
    public static function transformedAttribute($index)
    {
        $attributes = array_flip(self::$map);

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
