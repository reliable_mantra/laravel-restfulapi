<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show passport PersonalAccessTokens component
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTokens()
    {
        return view('home.personal-tokens');
    }

    /**
     * Show passport Clients component
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClients()
    {
        return view('home.personal-clients');
    }

    /**
     * Show passport AuthorizedClients component
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAuthorizedClients()
    {
        return view('home.authorized-clients');
    }
}
