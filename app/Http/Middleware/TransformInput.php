<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\ValidationException;

class TransformInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $transfomer)
    {
        $transformedInput = collect($request->request->all())->mapWithKeys(function ($value, $input) use ($transfomer) {
            return [$transfomer::originalAttribute($input) => $value];
        })->toArray();

        $request->replace($transformedInput);

        $response = $next($request);

        if (isset($response->exception) && $response->exception instanceof ValidationException) {
            $data = $response->getData();

            $data->error = collect($data->error)->mapWithKeys(function ($error, $field) use ($transfomer) {
                $transformedField = $transfomer::transformedAttribute($field);
                return [$transformedField => str_replace($field, $transformedField, $error)];
            })->toArray();

            $response->setData($data);
        }

        return $response;
    }
}
