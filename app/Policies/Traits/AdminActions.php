<?php

namespace App\Policies\Traits;

use App\User;

trait AdminActions
{
    /**
     * Give all rights to admin users
     *
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }
}
